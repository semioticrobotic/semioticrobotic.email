Zoom and Gloom
by Robert O'Toole

https://aeon.co/essays/how-empathy-and-creativity-can-re-humanise-videoconferencing

Robert O'Toole is a senior academic technologist and philosophy graduate at the University of Warwick, and a National Teaching Fellow of the Higher Education Academy. He specialises in design thinking: developing and implementing transformational design and technology strategies for education and business. He lives in Kenilworth, Warwickshire.

Technology exists to expand and sustain our capabilities. Therefore, doing technology well contributes to our hopes for leading an ethically good life: developing the right capabilities in the right ways – and using them for good ends.

In this essay, I will explore how the experience of videoconferencing points, in one way, towards the limits of human adaptability and, in the other, to a liberating human capability that we must collectively cultivate and sustain – as an innovative extension to the ethical framework described by the philosopher Martha Nussbaum in Creating Capabilities (2011).

The philosopher Martin Heidegger redefined being human as 'being there', amid the cultural, material and technical complex through which we make ourselves and are made by the world. He turned philosophy's attention towards mundane tools, materials and practices: hammers, nails, wood. Basic tech. This is the 'equipmental totality' through which our projects are materialised, and which brings meaning to our lives.

This world of things doesn't so much fit together into a totality. It temporarily aligns on journeys through time and space, snagging and slipping here and there. It's not a perfect vehicle, but it works for us. And we can enjoy it.

The feeling of being entangled in real things gives us our sense of relationality, tension and agency – key human experiences. Videoconferencing needs to enable those experiences, not block them. How can we fit technologies to this messy, complex, entangled humanity? Designing for entanglements is the answer. We need to understand entanglement and technology. Let's explore this further through the case of videoconferencing.

In Anthropology and/as Education (2018), Ingold richly describes the experience of being in academia – as researchers, teachers and students – as moving together intimately through a complex environment in which we continually draw attention to things for each other, so as to gradually enrich our shared understanding and capabilities. He calls this being in correspondence, as opposed to a linear transmission of information. Such relationships are established and smoothly maintained in the manner of people walking together, entering into spaces, marking transitions of focus, running through patterns of activity, noticing, gesturing towards and away from, in a way that gracefully manages the complexity of people and their differences. This comes so naturally to us in our everyday lives that we often don't notice it happening, and can find it hard to describe – all of the tacit details without which academic life, and learning, would be impossible.

Life is being squeezed through the low-bandwidth channel of webcam frames, text-chat streams, emojis and scheduled meeting time-slots. The simplifying effect of that compression acts only to increase anxiety, to make us all feel under pressure. This is not calm.

But the experience of videoconferencing that we have today wasn't what Weiser and Seely Brown imagined. Along with their vision for the UC era (which they dated, optimistically, between 2005 and 2020), they proposed a qualitative measure of its success: calm technology – the extent to which a technology works so well, fits so perfectly with human needs and habits, that it disappears from view.
