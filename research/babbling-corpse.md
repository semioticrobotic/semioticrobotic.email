# Babbling Corpse: Vaporwave and the Commodification of Ghosts by Grafton Tanner

## Introduction: Ghostly Encounters

> "It is a confusing and eerie sensation to feel social while alone, thronged with invisible entities whose presence is felt yet who appear wholly absent. These entities are our twenty-first century ghosts, shorn from their corporeal shells and set loose to glide through cyberspace at lightning speed and with startling precision." (p. ix)

> "We live in the manufactured illusion that we are still separate from our media extensions when in fact we are interlopers stumbling through a spectral world not for us" (p. x)

Twin ghosts: the ghosts of our incorporeal selves flying through cyberspace, the ghostlinss of the Web--and then the larger ghost of shared traumas and histories , "forgotten pasts and lost futures" (p. x)

"At the crux of our haunted culture is vaporwave" (p. x); "this book does not offer a definition of vaporwave, nor does it present a proper history of the genre" (p. xi); "vaporwave is the musical product of a culture plagued by trauma and regression in late capitalism" (p. xi); "vaporwave is critical of Western culture's preoccupation with the past, but it is not the only art form to call our collective regression into question" (p. xi); there is something here like a "vaporwave sensibility" (p. xi), which names a more general "desire to turn our fascinations and fantasies into  more disquieting forms, to suggest that not all is perfectly well, to remind us that maybe we have not been liberated in the Internet Age" (p. xi)

> "With unprecedented access to the Internet, the flattened desert where past, present, and future comingle, we find ourselves living in a state of atemporality, yearning for a time before the present" (p. xi)

That time, of course, is pre 9/11 (the day the 21st century began)

## Chapter 1: Spectral Presence

Theme: Electronic media, the uncanny, and reanimation (the spectrality of vaporwave)

Why do we associate electronic media with the uncanny? With spectral presences?

The uncanny exists in the rift between the spaces where we'd like to assume some unity (between the familiar and the not-so); uncanny objects "rob the familiar of its comfort" (p. 3); "in this gap lies the truly alien realm wherein the strangeness of the everyday rises up to present us with the unalterable reality that the world is not for us" (p. 3)

When we encounter haunted media we "contront the uncanny with all its gaps" (p. 5); "their maulfunction, actual or perceived, indicates a rift between them and their carried messages" (p. 5)

Sampling creates "ghost music" that "reanimates the dead"; these metaphors aren't just arbitrary; sampled music "is Franken-music, using various pieces of former wholes to create a new being" (p. 6); sampling "exposes gaps: gaps in authorship, continuity, and the information needed to determine originality" (pp. 7-8)

Repeating short samples is a vaporwave hallmark, "meant to be exhaustive and to walk that fine line between funny and uncanny, and listeing to an entire track can waver between transcendental elation and disengaged ennui" (p. 8)

> "The anonymity is part of the experience of listening to vaporwave. You're engaging with music that comes from nowhere, that can be attributed to no one or at best a faceless moniker, and resists easy analysis" (p. 13)

## Chapter 2: Erasing the Human

Theme: Fascination with androdecentrism, immediacy, timelessness, placelessness

Begin with a description of speculative realism and OOO; these movements signal "the Western world's fascination with dread" (p. 15); also key here is Thacker's work on _The Horror of Philosophy_

This is a fascination with the unthinkable, the end-time, the meaninglessness of it all; "no future can be imagined when we live in the horror of immediacy" (p. 16); the world is increasingly unthinkable (Thacker); "this nonhuman turn in the arts and philosophy rejects the exceptionalism of humanity and embraces the uncanny, unthinkable world all around us" (p. 16)

The art here is indicative of the accelerated Anthropocene too; "in each of these examples, the human element is absent" and "in its place are things, both profound and perfunctory" (p. 18); this might be an "object-oriented aestheitc" (p. 18); and artists like MACINTOSH PLUS, Infinity Frequencies, and INTERNET CLUB are right in the thick of it.

> "In addition to being a genre of ghosts, vaporwave is the sound of the outside world of things--electronic technology, mass-produced goods, non-places" (p. 18)

Vaporwave often defies description because it's principally affective; "that mixture of dread, nostalgia, and transcendnce" (Far Side Virtual); parallels here to Lovecraftian horror

The focus here is "anthrodecentric thought," the uncanny and the sheer _horror_ of the unfathomable (Lovecraft: "terrifying vistas of reality")

The timelessness and placelessness of it all is part of this aesthetic; "we are allowed access to everything all the time" and "all time periods, all settings--everything is up for grabs in the endless archive of the Internet" (p. 21)

(Here is an extended analysis of some Internet memes, including Too Many Cooks)

## Chapter 3: Lost Futures and Consumer Dreams

Theme: Mining the past for critique, postmodernism and hauntology, historicity

The past continues to haunt the present in the form of period nostalgia (the past mined again and again, right now the 80s); this is our "hauntology" (p. 30)

To understand haunted culture, we need to understand postmodernism (turning primarily to Jameson and Hutcheson)

> "Postmodernism calls attention to the reality that in the age of commodified culture, art has to be produced within the capitalist market; therefore, culture and industry always intertwine" (p. 32)

Hutcheson then further claims that postmodernism critiques the very systems it uses to dristribute itself to masses; Jameson agrees to a point (see p. 33); postmodernism turns history into a commodity (in an age of global capitalism)

It's different, though, from hauntology:

> "This shift in focus from mining the cultural past for pastiche or parody to mourning a lost future differentiates postmodernism from hauntology" (p. 35)

So hauntological art is political, but in a different way, "by illustrating the past's continual reminder of the future's failure in the form of haunting" (p. 36)

> "Vaporwave is the music of 'non-times' and 'non-places' because it is skeptical of what consumer culture has done to time and space" (p. 39)

> "Vaporwave recontextualizes elevator music by sampling snippets from he most prosaic musical rubbish and presenting them in a different way, forcing us to reconsider Muzak's subversive qualities and its inane catchiness—to intently _listen_ to the music that has been humming just outside our perception for decades. Vaporwave engages in an act of reframing ..." (p. 41).

Vaporwave "relies heavily on certain images that evoke life under capitalism, both past and present" (p. 43); that is, "the majority of vaporwave albums can be read as indictments of life under the sign of consumption" (p. 44)

> "What makes vaporwave unique as a new method of Internet-produced punk is its relationship to the sights and sounds of unrestrained capitalism. Vaporwave spits in the face of late capitalism and mocks the very methods used to sell us the things we don't need, all while problematizing our understanding of history" (p. 49)

## Chapter 4: Sick and Tired

Theme: Reproduction of the past in the present, cultural regression

Once again we're thinking of 9/11:

> "By exploding the world as we knew it, the September 11th attacks shocked us into a state of cultural regression. We have been living in that period ever since, plumbing the past for comforting sounds and songs, sounds from periphery and mundanity of daily life before the great unraveling at the start of this century" (p. 52)

Channeling Zizek here on 9/11 as encounter with the Real; that encounter prompted a noticable return to firm ideologies (ideological/symbolic discourses and systems); the shock (Klein) has never really worn off; this is mirrored in the clinging to "retromania" (p. 54)

The past is now infinitely and routinely reproduced in the present, and "as we consume as much information as the Internet can allow, we enter into a digital feeding frenzy where we fear the prospect of missing out on checking the latest Instagram post, reading teh latest music review, or hearing the latest single" (pp. 54-55); we "feed on media to a point beyond fullness" (p. 55), "never actually catching up with all the culture we feel we must keep up with in the first place" (p. 55); this is "a lonely, exhausting burden unique to our moment in history" (p. 55)

> "Vaporwave is an excellent example of just how commodified the ghosts of our past are. The earliest vaporwave producers sought to recontextualize our insatiable hunger for the past by delivering nostalgia in its remembered form--hazy, looping, distorted, unclear--all while mocking and subverting the entire process" (p. 60)

The purpose of this book was to "take a hard look at a tiny flashpoint in culture, the birth of vaporwave, and situate it within our contemporary moment--one characterized by uncanny ghosts, historical trauma, regression, simulation, and a nebulous strangeness that seems to hum menacingly underneath it all" (p. 70)

We cannot forget vaporwave's critique of capitalism and consumerism, "its jarring indictment of consumerist culture," (p. 70); vaporwave "accomplishes something contemporary music rarely does" (p. 71); namely, it "invites us to react emotionally to a genre of music that has subversive potential" (p. 71)

> "For now, we live in the mall, but I think it's closing soon" (p. 71)
