# Notes on Non-Places

Is this ""vapor theory"? Vaporwave is not a postmodern aesthetic but a _supermodern_ one (explore this!).

## Tanner, Babbling Corpse

Tanner invokes Auge on page 38

## Auge, Non-Places

## Reading Notes

# Non-places

Emphasis here is on "over-abundance" of events, meanings.

> This overabundance, which can be properly appreciated only by bearing in mind both our overabundant information and the growing tangle of interdependencies in what some already call the 'world system," causes undeniable difficulties to historians, especially historians of the contemporary... (p. 23)

Excess is the sine qua non of supermodernity; specifically, an excess of meaning is what we're experiening ; "this need to give meaning to the present, if not the past, is the price we pay for the overabundance of events corresponding to a situation we coiuld call 'supermodern' to express its essential quality: excess" (p. 24); supermodernity is the obverse side of a coin with postmodernity, "the positive of the negative" (p. 25)

> From the viewpoint of supermodernity, the difficulty of thinking about time stems from the overabundance of events in the contemporary world, not from the collapse of an idea of progress (p. 25)

> If a place can be defined as relational, historical, and concerned with identity, then a space which cannot be defined as relational, historical, or concerned with identity will be a non-place (p. 63)

Supermodernity produces non-places; super-modernity is characterized by excesses of time, space, and ego.

"Alone, but one of many, the user of a non-place is in contractual relations with it (or the powers that govern it)" (p. 82; the non-place is to be used; "a person entering a non-place is relieved of his usual detriments" (p. 83); that is, "he becomes no more than what he does or experiences in the role of passenger, customer or driver" (p. 83); "the space of non-place creates neither singular identity nor relations; only solitude and similitude" (p. 83); "what reigns there is actually the urgency of the present moment" (p. 83); "everything procedes as if space had been trapped by time, as if there were no history other than the last forty-eight hours of news" (p. 84)

## Notes from seminar with Sarah Sharma (January 26, 2012)

(The book was published in 1992, then saw English translation in 1995 and reprinted through the mid-2000s)

Can we do an ethnology of France? Of contemporary life? Yes. That's the book. What is the evidence for these claims? What about the data he cites?

Sarah: Every time I read this book, I get something different out of it. It has a large following in media studies, but it's also an intervention in anthropology. If the question of anthropology is always the Other, what do we do when the Other can no longer be the subject of supermodernity? At first you read this and see it as a critique of non-place as the Other of public space. Don't read non-place as the polar opposite of anthropological space, but as an extension of anthropological place where there's a lack of a referent. Anthropological place and non-place seem to be either a dialectic or a continuation of one another. It's not an Other that localizes a traveler, but a ritual (performing passenger); it brings relief and even joy sometimes. He's writing this at a time when others are writing critiques of anthropology between the 70s and the 90s. Auge is very different, trying to do away with lots of dualisms. Needs to come up with a way to understand our social relationships, without relying on self/other relationships. This has been the sole object of anthropology—this relation. How to do an anthropology of supermoderity when history can no longer be the discipline de jure, when there's an abundance of events? The things that history has relied on are falling away, morphing and becoming unruly. He's looking for a method for an ethnology of supermodernity. (Cultural studies might provide the answer to this, by focusing on labor, etc.). It's worth lingering here, thinking about doing an anthropology of supermodernity that doesn't rely on self-other distinction. Anthropology needs to deal with the privileged temporal space of its observer. So Auge shows us that even these interventions are problematic. Rather than putting the native under the glass, we have a rather ambivalent Other, nothing exotic! How to reconcile anthropology with supermodernity? By pointing out the supermoern condition, you destabilize the entire field of anthropology. Need new questions, new tools, etc.

We can read this as a lament of non-place (nostalgic): this is not about the negation of identity or narrative, but that it's multiplying in its excess. Multiplication of space, time, and ego—these are now in abundance. It's not that there are no referents, but there are _different_ referents that put you in the non-place. So where can we look for these? That's the new anthropological project. You can't just write off super-modernity. So non-place attempts to hide this relationship; its architecture hides it. You might walk in, in the first instance, and see clean lines, flows, etc.—but it's hiding the multiplication of all this, all these things. So what type of person is realized in non-place? It's not that there's no Other; it's that there's a new focus on the Other (it's not the main object of an anthropology); it's not the sole focus of the project.

Then there's the issue of identity: People say Auge's wrong. Even if you're an asshole or a bigot, you don't change just because you enter an airport! But it's really an oblique critique. It's not about identity, but modes of identification.

Discussion: There's really an emphasis on liminality here.

Remember: postmodernity is like a negation of everything, but supermodernity is the positive side of postmodernity, the excess (not the negation of everything); non-place is not a place (though "non-places" like the airport are concrete edifices of non-place); non-place is a condition.

The abundance of meaning is also an abundance of _time_: there's a desire for things to have more meaning.

[BREAK]

The issue of commodification: in one reading of non-place, the old question of anthropology would be the one of authenticity/inauthentic (commodified); the non-place commodifies difference, commodifies Otherness; Auge says we need another lens (can't just write off the food court as inauthentic!); David Harvey gives us another lens with which to examine this: the global timespace of the world (and global capital) still requires a spatial fix (it's not as though non-places are inauthentic but authentic to their purposes, as the empirical sites at which capital functions)


