# Nostalgia Reimagined 

https://aeon.co/essays/nostalgia-doesnt-need-real-memories-an-imagined-past-works-as-well

by Felipe De Brigard, Fuchsberg-Levine Family Associate Professor of Philosophy, and associate professor in the departments of psychology and neuroscience, and the Center for Cognitive Neuroscience at Duke University. He is also principal investigator of the Imagination and Modal Cognition Laboratory within the Duke Institute for Brain Sciences.

"More interesting still is that nostalgia can bring to mind time-periods we didn't directly experience."

"In fact, a new word has been coined to capture this precise variant of nostalgia–anemoia, defined by the Urban Dictionary and the Dictionary of Obscure Sorrows as ‘nostalgia for a time you've never known'."

"How can we make sense of the fact that people feel nostalgia not only for past experiences but also for generic time periods? My suggestion, inspired by recent evidence from cognitive psychology and neuroscience, is that the variety of nostalgia's objects is explained by the fact that its cognitive component is not an autobiographical memory, but a mental simulation – an imagination, if you will – of which episodic recollections are a sub-class. To support this claim, I need first to discuss some developments in the science of memory and imagination."

"Almost 100 years later, and breaking with the psychoanalytic tradition, the American psychiatrist Jack Kleiner reported the case of a profoundly nostalgic patient that nonetheless exhibited joy, leading Kleiner to propose a difference between homesickness and nostalgia, on the grounds that the latter involves both sadness and joy. This distinction was later reformulated as depressive versus non-depressive nostalgia, with some even suggesting that the abnormal case of nostalgia is the depressive one, given that its pleasurable aspect is missing."

"But what about all these negatively valenced symptoms – the sadness, the depression – associated with nostalgia? Aren't they also effects of nostalgia? My sense is that physicians of old got the order of causation backwards: nostalgia doesn't cause negative affect but, rather, is caused by negative affect. Evidence for this claim comes from a number of recent studies showing that people are more likely to feel nostalgia when they are experiencing negative affect. Specifically, it has been documented that certain negative experiences tend to trigger nostalgia, including loneliness, loss of social connections, sense of meaninglessness, boredom, even cold temperatures. This doesn't mean that nostalgia is triggered only by negative experiences, but it does suggest that the negative affect can often be a cause, rather than an effect, of nostalgia."

"But when attention is focused only on the content of the counterfactual thought, not to the situation one's in when simulating, negative emotions can ensue. Consequently, the discrepancy between the emotion felt when attending to the act of simulating versus the content of the simulation can account for the perceived ‘bittersweetness' of nostalgia."

"A possible solution is to think of the object of nostalgia's desire as a place-in-time. This strategy allows for two possible readings. On one reading, what the individual desires is for her current self to travel back in time to where things were better than they currently are. This is painful because time-travel is impossible. On another reading, what the subject desires is for the past situation to be brought to the present; that is, she doesn't wish to travel back in time to a past situation, but rather that the past situation could somehow replace the current one. Here, the object that could satisfy the nostalgic desire would be found not in the past but in the present, and what causes the pain is a different kind of impossibility: that of recreating the past in the present."

"Now recall my earlier discussion of the discrepancy in the valence felt when attention is directed to the simulated content versus the act of simulating. My proposal here is that what underlies the motivational aspect of nostalgia comes from a pleasurable reward signal that the subject momentarily experiences when attention is allocated to the simulated content. As it turns out, this is exactly what the neuroscientist Kentaro Oba and colleagues in Tokyo found in a 2016 study, where brain activity in regions associated with reward-seeking and motivation was higher during nostalgic recollection. Entertaining the kinds of mental simulations that elicit the bittersweet feeling of nostalgia generates a reward signal that seems to motivate individuals to turn their ersatz experience into a real one, in an attempt to replace the (actual) negative emotion felt when simulating with the (imagined) positive emotion of the simulated content."

"Nostalgia, then, is a complex mental state with three components: a cognitive, an affective, and a conative component. This is generally recognised. However, my characterisation differs from the traditional one in putting imagination at its heart. First, I suggest that the cognitive component needn't be a memory but a kind of imagination, of which episodic autobiographical memories are a case. Second, nostalgia is affectively mixed-valanced, which results from the juxtaposition of the affect generated by the act of simulating – which is typically negative – with the affect elicited by the simulated content, which is typically positive. Finally, the conative component isn't a desire to go back to the past but, rather, a motivation to reinstate in the present the properties of the simulated content that, when attended to, make us feel good."

"For the politics of nostalgia doesn't capitalise on people's memories of particular past events they might have experienced. Instead, it makes use of propaganda about the way things were, in order to provide people with the right episodic materials to conjure up imaginations of possible scenarios that most likely never happened."

# The Way We Never Were

https://newrepublic.com/article/132001/way-never

https://www.amazon.com/Way-We-Never-Were-Nostalgia/dp/0465090974

"Nostalgia is a very human trait. When school children returning from summer vacation are asked to name good and bad things about their summer, the lists tend to be equally long. As the year goes on, however, if the exercise is repeated, the good list grows longer and the bad list gets shorter, until by the end of the year the children are describing not their actual vacations but their idealized image of "vacation." So it is with our collective "memory" of family life. As time passes, the actual complexity of our history—even of our own personal experience—gets buried under the weight of the ideal image."

"Selective memory is not a bad thing when it leads children to forget the arguments in the back seat of the car and to look forward to their next vacation. But it's a serious problem when it leads grown-ups to try to recreate a past that either never existed at all or whose seemingly attractive features were inextricably linked to injustices and restrictions on liberty that few Americans would tolerate today."

# Why We Reach for Nostalgia in Times of Crisis

https://www.nytimes.com/2020/07/28/smarter-living/coronavirus-nostalgia.html


# Also find nostalgia and privilege (Ready Player One)

https://www.vox.com/culture/2018/3/26/17148350/ready-player-one-book-backlash-controversy-gamergate-explained

https://sojo.net/articles/ready-player-one-confuses-meaningful-nostalgia-legalistic-knowledge


