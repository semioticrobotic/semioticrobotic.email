# The 9/11 Era Is Over

https://www.theatlantic.com/ideas/archive/2020/04/its-not-september-12-anymore/609502

"Now, as COVID-19 has transformed the way that Americans live, and threatens to claim exponentially more lives than any terrorist has, it is time to finally end the chapter of our history that began on September 11, 2001."

"To have this unfathomable event framed in a way that fit neatly into the American narrative that I’d grown up with in the 1980s and '90s was reassuring. After a decade defined by the triviality of Bill Clinton’s impeachment and the absence of a sense of mission, America had a new national purpose on par with the Cold War—another generational effort to make the world safe for democracy."

"But by the end of the Bush presidency, as the global economy collapsed around me, it was impossible to ignore the fact that America’s response to 9/11 had done more harm than good."

"In his attitude and approach, Trump himself remains very much a president of the 9/11 era. He could not have become president without the architecture of right-wing media, chiefly Fox News, that blossomed after the attacks of 9/11. His personal lawyer Rudy Giuliani turned his laudable response to those attacks into a career of profiteering that led him all the way to Ukraine in pursuit of conspiracy theories about Joe Biden. Trump’s lie that Muslims in New Jersey celebrated the fall of the Twin Towers completes the distortion of that day from a moment of American common purpose to an expression of white identity politics against an encroaching 'other.'"

"Enormous upheaval, however, also offers the opportunity for enormous change. And that is what America needs. This is not simply a matter of winding down the remaining 9/11 wars—we need a transformation of what has been our whole way of looking at the world since 9/11. Yes, we have a continued need to fight terrorist groups, but the greatest threats we face going forward will come not from groups like al-Qaeda or ISIS, but from climate change, pandemics, the risks posed by emerging technologies, and the spread of a blend of nationalist authoritarianism and Chinese-style totalitarianism that could transform the way human beings live in every country, including our own."
