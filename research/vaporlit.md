What is "vaporfiction" or "vaporfic"? What's required for it?

- Is it something diegetic—a setting?
- Is it a pallet—a mood?
- Is it a tone—a sense of ennui?
- Is it a structure—a lack of finality?

Listeners don't listen to vaporwave for any sense of completion or finality—quite the opposite, in fact. The same goes for vaporfic.

## Reading List

Enjoying It (Bown)
Generation X (Coupland)
Close to the Machine (Ullman)
Simulations (Baudrillard)
