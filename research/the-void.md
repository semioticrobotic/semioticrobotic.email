# The Void
Isaiah Laing and Alexis Sundquist

Aurora grabbed a slice from the fridge and sat down. Hazel wrapped an arm around Aurora and turned the TV on to mute the silence.
LOCATION: 14

She tried to reacquaint herself with her surroundings. The world was distorted, as if being seen through a tube TV. The ground was a strange plane of fuschia tiles separated by thin silver lines. Above her was a void of black, speckled with bright white dots. The only things inhabiting this place were palm trees glowing with a strange aura. She was far from home.
LOCATION: 21

She clicked the radio on. A strange night time station was channeled in. The slow, melodic beat was hard to make out over the crackling of the broken transmission. Something about it disturbed her, and she quickly changed the station.
LOCATION: 46

The scene before her made her feel small and empty, but she was used to that. It was breathtaking and crushing. It was a moment of chaotic beauty that she lived for.
LOCATION: 51

She was finding herself in the Void more often. It gave her a sort of hollow comfort. It kept her emotions away. She didn’t have to feel.
LOCATION: 63

Charlie woke up in the parking garage. She approached her corpse. A deeply unsettling feeling formed in the pit of her stomach and she froze for a moment. The corpse’s eyes burst open, and it sat up slowly. Charlie and the corpse locked eyes. Everything around the two of them seemed to be consumed in TV static. A terrible noise blasted into their ears. They reached out. Slowly. As they made contact-.
LOCATION: 190

Nova gazed vacantly out the car window. Gas station in five miles. Exit 224. His hands shook and he had to tightly grip the steering wheel to keep them still. He took his phone off the dash and glanced at the screen.
LOCATION: 194

He squinted as his eyes adjusted to the hazy white light of the store. It was small, choked with smoke, and filthy.
LOCATION: 197

He was getting close now. He could feel his heart in his throat. Shallow breaths. Eyes burning. Tight chest. He turned on the radio, trying to drown out the deafening silence. Distorted jazz emitted from the cheap speakers. The darkened cityscape surrounding him was unfamiliar.
LOCATION: 203

Ember threw on the last clean clothes left in her suitcase and sighed. It was going to be a long night and she didn’t expect to get any sleep, so she decided to head to the laundromat. The harsh yellow lights, odd carpet pattern, and claustrophobic narrowness of the hallway that greeted Ember outside her room caused her eyes to ache. The suitcase rumbled along as she entered the elevator. She pushed a button on the dimly lit keypad, which seemed to be covered in something orange and sticky. It crept down with an unsteady creak. The strange sensation of the elevator sliding down made her stomach uneasy. A jazzy series of muffled crackles and distorted saxophone notes dripped out of the elevator’s speaker.
LOCATION: 220

Nova looked around. It was a small place. Almost all the light in the room could be attributed to the neon sings littering the brick walls, depicting bottles of beer, palm trees, a few different brands of alcohol, and such. A jukebox sat idly in the corner, complimenting the retro, bright red upholstery of the booths. A few refurbished arcade machines lined the wall to their right.
LOCATION: 295

She collapsed back into bed, picking her phone back up and calling someone. “What is it?” the person on the other side spat. “Girl troubles again.” Jackie sighed. “Can we talk about this tomorrow?” “Come on Roxy, I know you haven’t gone to bed yet anyway.” “Yeah, yeah, I was at a party. I met some guy there who was pretty nice though,” Roxy said.
LOCATION: 333

She would be meeting with Hazel later at her apartment, and until then the day seemed to drag on endlessly. Not much food got down before her appetite was gone. There wasn’t much point in leaving the house, nothing good seemed to be on TV, and after losing focus trying to read a manga, she put on music to drown out the silence. She checked her clock, walked around, checked the clock. Only moments would pass before the pattern repeated. Sadie was alone, and loneliness is unforgiving.
LOCATION: 771

She slammed on it harder and it burst open. Kicking it closed behind her, she sprinted down a hall and through a wide set of glass doors. Beyond them was huge, open pool room. It was dark, save for the water, glowing a neon purple from lights deep in the bottom of the pool.
LOCATION: 854

The ground beneath her was mirror-flat, a strange neon fuchsia. Thin silver lines crossed to make large tiles on the floor. Above her was a void of black, speckled with bright white dots. The city before her was similar to home, but it didn’t feel right. She walked through the empty streets. Nobody. She entered a nearby store. A clerk sat at the counter. No. Where one would be sat an expressionless marble statue. There was another standing in one of the aisles, posed with a shopping cart. Uneasy, she stepped out and walked down the street. The buildings were familiar.
LOCATION: 861

A faint glow began to grow ahead, gently dripping into their fields of vision before enveloping the group as they entered a new room. The room was strange, coloured in shades of pink and blue. Every machine screen was covered with an “Out Of Order” sign. Except one. Its screen displayed TV static, emitting an awful ringing.
LOCATION: 917

Hazel pushed through the glass doors and stared out into the darkness. It was not home. Before her was a vast desert. Thick, suffocating heat enveloped her as she walked out of the air conditioned arcade. On the horizon was the massive shape of a distant planet. Hazel felt it calling out to her, drawing her towards it. A few steps towards it, then she fell to her knees and kept her eyes peeled open, fixed on the celestial body.
LOCATION: 922

Roxy pulled up to the front of the arcade in the seat of a black, coughing jalopy. “The keys were sitting right on the dash.” Snow shrugged and climbed into one of the seats in the back row. Sadie followed, and Hazel took the front passenger seat. The interior was lined with aged red leather, and the radio had a slot for cassettes. Roxy flipped a switch and the headlights coughed to life, fighting to break through the pitch black night.
LOCATION: 935

After a quick trip back into the department store, she dropped the skateboard onto the concrete and kicked off. Warm gusts of wind breezed through her hair as she rode down the empty streets and alleyways.
LOCATION: 949

Emptiness edged closer into her brain with the cold realization she was truly alone in this place. Beyond the city she could see nothing but miles of that mirror-flat, artificial ground, littered with fake plastic trees, wide palm leaves glowing unnaturally.
LOCATION: 964

The house, more of a mansion really, loomed above the group as they exited the car. It was a very modern-looking building, comprised of mostly straight lines and rectangular shapes, glossy white walls, and massive windows.
LOCATION: 1110

Hazel’s vision was hazy, as if she were seeing the world through a tube TV. The ground beneath her was mirror-flat, a strange neon fuchsia. Thin silver lines crossed to make large squares on the floor. Above her was a void of black, speckled with bright white dots. The only thing inhabiting this place was palm trees. They had a strange neon glow. She was far from home.
LOCATION: 1163

Time did not work properly in this place, nothing did. They were walking along some sort of fault line in time and space where the two had collided and at the epicentre of the impact created some sort of void where neither time nor space filled. The longer they walked the more their bodies felt as if they were separating from their souls, spirits traversing a place uninhabitable.
LOCATION: 1171
