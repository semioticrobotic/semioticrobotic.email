# NOJOY
H. William Davis

The bus stop is a place people only visit in need of somewhere else to be.
LOCATION: 81

In college she experimented with piercings only to find septum rings and gauges littering her bed the next morning. She would dye her hair in shades of pink and blue and purple only to wake to her normal blonde locks. Any attempt to alter or mar her body reverts overnight. Margaret is incorruptible.
LOCATION: 121

Lawrenceville is vocal, ambient, audible. Sounds intertwine, become something more sonically profound than their individual parts.
LOCATION: 301

She rests her pen at the treble clef. Notes echo through her mind. Cecilia can't force them out of her heart, through her arm into the pen and onto the page. She can't write music. She doesn't understand the correlation between the physical and the audible. She doesn't understand how one captures a melody.
LOCATION: 305

There is no joy in shifting around the dirty city, heat pouring down in thick damp waves, searching for something he didn't lose in the first place.
LOCATION: 340

Anthony leans in and listens to Allie, where the jacket came from, the girl he was with when he got it, the girl who stole it from him, playing in punk bands in the 70's and 80's, coming to America. They share an avid distrust of Green Day and Hot Topic. Then Anthony feels the tug on his brain, like someone has their fist around his cerebral cortex, yanking it down his spinal cord.
LOCATION: 372

The Orthodox clump in roadways, navigating the street as if it were filled with shadows, passing through tall dark ranks of businessmen.
LOCATION: 422

Francis walks over to the bum. He grabs at the ground with long, dirty fingernails, sifting through pebbles to stack up bills. Francis hunkers down next to him and more people throw money his way. A line is formed to give the guy money.
LOCATION: 438

August palms a strange green bottle covered in etched fairies, the label sprawled with indecipherable French writing. He can't help but pry the cork from its dusty neck. The bar is filled with a strange smell, the perfume of booze wafting into August's nostrils. August has never been drunk, but before he even lifts the bottle to his lips he feels something he has never felt before, a gentle tug of the brain saying yes, please, more. The liquor doesn't have a strong burn of alcohol, but the flowery, old-person candy taste to the stuff makes August reel.
LOCATION: 496

She imagines them planning their lives, everything set out before them. She sees engineering students and accounting students and children of all kinds who have no trouble expressing the thoughts in their heads.  She envies them.
LOCATION: 572

"Bummer," says Margaret. Cecilia stares past the rooftops of Lawrenceville, into the hazy, screen printed sun, the city muted in shades of pink and blue.
LOCATION: 619

Margaret laughs as Frank sulks, sifting through papers, telling them where to sign as the sun shifts in the sky, light darting from one corner to another, silhouetting them like performers in a play, the picture window a stage for the whole city.
LOCATION: 740

He heads out into the gloomy, heat drenched sun of the Pittsburgh evening. Christmasland sits abandoned and lonely across from the New Amsterdam. It looks sad without the splattering of blinking neon. This is August's life, the neon turned off, the glamour erased, the bare bones of his foundation at his feet, ready to either mold and fester or to be sculpted into glory. August doesn't know what it takes to shape a life, but he can see the pieces laying all around him, the instructions blown to the wind, caught in the trail of a fleeing bus.
LOCATION: 792

None of this makes her feel better, of course, but my god, the success, the life that can unravel from a perfectly cooked french fry.
LOCATION: 847

There is no joy in the woman's voice, nor on her face. She is doing a job. The customers, Cecilia and the people before her, the people after her, are her job, a line of dominoes needing to be pushed down.
LOCATION: 859

The cashier hopes that Cecilia's day and whole life are as filled with NOJOY as her own.
LOCATION: 862

In the dark recess of Bartlett, Cecilia feels the NOJOY the Rite Aid woman put on her.
LOCATION: 866

"You haven't seen my phone," Margaret laughs. "You don't have one of those fancy things with the games and tweets and shit?" "Nah," says Margaret, holding up a Nokia brick, its single game, Snake, boring since 2001. "Damn," Juliana says. "You gotta get yourself an upgrade. That shit's obsolete as fuck." "I don't need all that shit," Margaret says. "Everyone else your age seems to," Juliana says. "They're all fucktwits," Margaret scowls, "I hate those motherfuckers. Make us all look like screen-obsessed nitwits." "Wow,” says Juliana, "Maybe you all aren't doomed after all."
LOCATION: 1023

He feels like he's a trespasser on this person's day, forced to move around, gathering things for others, no joy issuing from that secret smile they reserve for off-work hours. Francis pays the clerk and scuttles out of the store, standing under the roof to light his smoke.
LOCATION: 1084

He looks ridiculous in his work gear, ripped Levi's, dirty white shirt promoting some skateboard brand, a pair of floppy Converse. This is what he would wear if it were his friends building a bench or some shit on a drunken weekend. The tools, the wood, the huge men, all came unexpected.
LOCATION: 1116

Pete is alone, even though people dash around him. This is a machine, the workers moving parts.
LOCATION: 1218

What's inside a McDonald's when the people are gone? Silence, annoyance, hopelessness, thick in the grease-stained air.
LOCATION: 1219

The world is full of music, the cadence of footfall, lingering chords of a busker, the static of idling engines. Cecilia can't make out a melody. There's not enough.
LOCATION: 1229

Urgency, movement, not panic but meaning, understanding, sound vibrating through the air, everything in its right place, perfect, predestined, inevitable.
LOCATION: 1236

Shadows leak into Allie's car, a beat-up Ford Escort spitting fumes into the night sky. Darkness flashes between streetlights like the snap of a camera lens. Big houses and shopping complexes fall away, replaced by ratty convenience stores where people pump gas under a canopy of florescent lighting.
LOCATION: 1286

Not all rebellion has to be aggressive. Some rebellions are slow and sad and soft. Anthony can hear the music. He can really hear the music.
LOCATION: 1350

"You know, Jules," Margaret says. "I never know how to help people, it hardly ever works out. But I always try."
LOCATION: 1402

"My dear, in a world full of the mediocre, that makes you a saint." Margaret laughs and then Juliana laughs too and the wind adds its own cackle of slapping leaves against the windowsill.
LOCATION: 1405

His day continues like this, a life made full with bits of nothing. From above you can watch him move, a stream of commerce trailing him, people drawn to spend their money at hole-in-the-wall places that should have gone under years ago.
LOCATION: 1463

To inspire charity is grace. Francis's grace is empty. He causes money to move, but he can't make it fair.
LOCATION: 1465

Who wouldn't love to sit with their loved ones all day, basking in the gloomy sun of Pittsburgh, or to watch the races undisturbed, or to not be a wage slave, tied to a job that brings no joy.
LOCATION: 1469

They look each other in the face, day after day after day, only legal tender exchanged, no forlorn glances, no smiles of exultation, no joy.
LOCATION: 1473

It's an awful thing to lose a home. It's an awful sacrifice to have to make. Peter walks down Penn Avenue. There's a mist in the air. Suddenly Peter can smell french fries. He can feel grease in the air. The smell makes him sick. The grease sticks to his sweaty skin. Something is happening here.
LOCATION: 1560

This isn't about a group of misfits needing a mother. Juliana isn't matronly.  She's one of them, on another side of time.
LOCATION: 1588

You spend your whole life trying to remember these little things that trigger your inspiration, a cup of coffee at a certain cafe, smoking a cigarette on a fire escape somewhere, a certain person or thing, another work of art, an important book, you start to think your creativity is bound to these things. You rely on them, when they provide nothing, really.”
LOCATION: 1621

The coffee pulsing through his veins pulls his gaze out the window, monoliths of buildings waiting for someone to walk by, the streets lonely. The night is static and unmoving beyond the window. This place feels so much bigger. How big can a ghost be?
LOCATION: 1655

August wakes, grabs his phone, scrolls through missed calls, junk emails, voicemails from debt collectors. He sips stale water, calls James back.
LOCATION: 1895

He scoops grounds into a paper filter. Birds chirp between the rumble of trucks tearing up asphalt. The coffee pot gurgles brown fluid into a glass carafe. Drips of coffee sizzle on the burner. Light filters through the curtains. Everything glows with silence.
LOCATION: 1899

“Listen,” Thomas says. “My parent's house burnt to the ground a week ago. They lost everything. I'm a fat, twenty-something, loser who lives in their basement. I lost everything too. I still do my job, and I'm still this guy's trainer for the McDonald's Olympics, and I came here to help him, after we've already worked an eight-hour shift.” Thomas pants, recouping his breath. “Your boss needs to do his job.”
LOCATION: 2013

Pink and gray light spills over the horizon. Hillsides are topped with cotton candy glow.
LOCATION: 2041

Cecilia imagines him blushing under his beard. She wants to stare into his eyes and hear his words. But she doesn't. She leans forward and tries to make out his mumbled chorus. All she can catch is one phrase, repeated ad nauseam, no joy, no joy, no joy.
LOCATION: 2107

Cars move from red light to red light. Fumes clot the streets. The noxious smell of burnt gas mixes with fresh bread, ethnic food, the sizzling waft of pizza ovens. Cars park illegally, ignoring the faded yellow bars on the sides of the streets.
LOCATION: 2237

He knows what kind of people find their way into this job and never find their way out. They aren't bad people. They're people with real skills and genuine attitudes and a kind of work ethic that seems extinct. They don't know how to use those things. They don't realize they are things. They just are the way they are.
LOCATION: 2902

Christopher laughs, too much. Margaret's smile is a thinly-veiled gesture of amused disdain.
LOCATION: 3053

“You can acknowledge it but you can't change your habits.” “I think we all kind of end up where we're supposed to anyway.” “Like divine intervention or something?” Peter says. “Nah, not like that,” Thomas says, shaking his head. “Just like, who you are and where you're at just sorta add up to something, eventual, I guess.” “Genetics then?” Peter says. “Nature over nurture?”
LOCATION: 3270

“No, not that either,” Thomas says. “I can't get it across without sounding dumb, but I guess somewhere in between, like fate and DNA intertwine into something that opens up places in the world for us.” “I didn't know you had thoughts that deep about anything other than how to assemble a Quarter Pounder,” Peter laughs. “It's nice to be out of that place,” Thomas sighs. “A huge weight is gone,” Peter says.
LOCATION: 3275

“You know,” Denis says, staring off into rows of gravestones, “I thought about waiting for you the first time I left the book.”  “That book belonged to someone else.” “No,” Denis says, shaking his head. “No, it was mine. He now has two copies. But a man who loses things never really knows what's his in the first place.”
LOCATION: 3413

He's falling into patterns. Rituals keep him company now that the old man is gone. The Bonnie Raitt record is gone. He's replaced it with a TV On The Radio album. A slow, sultry tune weaves through the vapory air. The old Windows ‘98 machine is gone, replaced with an iMac.
LOCATION: 3522

The space is new and old. August isn't sure where he is at times, the past or future. The present is too hasty and vast.
LOCATION: 3524

Beneath the hardwood floors, shining and new, lies musty old wood. How many times have they walked across that floor? A polished surface dictates nothing. Inside the walls are older walls, walls that have heard them all laugh and cry.
LOCATION: 3770

Peter sits on the stack of pallets, wheezing through a Marlboro Red. Everything is actualized. The wheels are free to turn on their own accord.
LOCATION: 3845

There's hipsters with ironic t-shirts and old ladies with hymn books. There's homeless men and women taking shots with bankers and business owners. All of Pittsburgh is here, in Christmasland, bursting out the doors. Everyone beams with happy, joyful, faces. Everyone and everything, all of Pittsburgh, glows and vibrates, bodies full of August's blood and Peter's flesh, ears attuned to the stifled chords of Cecilia's soul, eyes glued to Margaret's mural. They were brought here by Anthony and Francis, the lost and the found, everyone at home in this place of rebirth. Cecilia understands. It's time.
LOCATION: 4121

She walks the streets of Lawrenceville, Cecilia's song in her mind, August's drink on her tongue, Peter's food in her stomach, Anthony's longing in her limbs, and Francis's kindness in her heart. They are all a part of her. They are all permanent.
LOCATION: 4340

They move through the city and with their footsteps make change. Ground is made sacred. People are healed. Joy finds its way into the hearts of those who have hardened from it. Their actions, like all actions, cause reactions. They form a web of joy through the city, connecting its people and each other. They are pulled from one path to another, setting into place a series of event made necessary by the jolt of the real world. Paths wind back to the beginning, to a fountain in a park full of lost keys.
LOCATION: 4386
